package pl.jazapisek.msources;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MsourcesApplication {

    public static void main(String[] args) {
        SpringApplication.run(MsourcesApplication.class, args);
    }
}
