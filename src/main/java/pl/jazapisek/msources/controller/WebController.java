/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.jazapisek.msources.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * http://www.thejavageek.com/2017/02/25/spring-data-jpa-query-methods/      TO ciekawy link
 * 
 * 
 * @author lfimariuszzap
 */
@Controller
public class WebController {
    
    @RequestMapping("/test")
    @ResponseBody
    public String test() {
        return "ok";
    }
    
}
